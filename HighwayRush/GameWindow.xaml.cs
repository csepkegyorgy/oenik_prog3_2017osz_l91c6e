﻿// <copyright file="GameWindow.xaml.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush
{
    using HighwayRush.Core;
    using HighwayRush.Core.Entities;
    using HighwayRush.View;
    using System.Windows;

    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// The window shows the game itself.
    /// </summary>
    public partial class GameWindow : Window
    {
        GameModel model;
        GameLogic logic;

        /// <summary>
        /// Base constructor for GameWindowViewModel class receiving viewmodel as parameter
        /// </summary>
        /// <param name="vm">Viewmodel containing information for the game initialization</param>
        public GameWindow(GameWindowViewModel vm)
        {
            model = new GameModel(vm.CarColorType);
            logic = new GameLogic(model);
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GameControlEntity.SetData(model, logic);
        }
    }
}
