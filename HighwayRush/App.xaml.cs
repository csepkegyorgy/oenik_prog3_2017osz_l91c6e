﻿// <copyright file="App.xaml.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}
