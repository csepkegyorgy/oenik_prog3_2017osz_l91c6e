﻿// <copyright file="Config.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Resources
{
    using System.Windows.Media;

    /// <summary>
    /// The static config class defining configuration data for the game.
    /// </summary>
    public static class Config
    {
        private const int lanesNumber = 5;
        private const int laneWidth = 80;
        private const int screenWidth = 600;
        private const int screenHeight = 600;
        private const int mainMenuControlWidth = 500;
        private const int mainMenuControlHeight = 500;
        private const int maxVelocity = 1500;
        private const int breakPower = 30;
        private const int velocityMultiplier = 10;
        private const int followUpDistance = 50;
        private const int vehiclesVelocity = 500;
        private const int frontalCollisionThreshold = 200;
        private const int sidewayCollisionThreshold = 800;
        private const int thresholdVelocity = 500;
        private const double fps = 50;
        private static Brush backgroundColor = Brushes.Green;
        private static Brush lanesColor = Brushes.Gray;

        /// <summary>
        /// Background color of the screen.
        /// </summary>
        public static Brush BackgroundColor { get => backgroundColor; set => backgroundColor = value; }
        /// <summary>
        /// The color of the lanes.
        /// </summary>
        public static Brush LanesColor { get => lanesColor; set => lanesColor = value; }
        /// <summary>
        /// Frames per second of the game.
        /// </summary>
        public static double Fps => fps;
        /// <summary>
        /// Threshold velocity for score gaining.
        /// </summary>
        public static int ThresholdVelocity => thresholdVelocity;
        /// <summary>
        /// Sideway collision threshold.
        /// </summary>
        public static int SidewayCollisionThreshold => sidewayCollisionThreshold;
        /// <summary>
        /// Frontal collision threshold.
        /// </summary>
        public static int FrontalCollisionThreshold => frontalCollisionThreshold;
        /// <summary>
        /// The traffic's velocity.
        /// </summary>
        public static int VehiclesVelocity => vehiclesVelocity;
        /// <summary>
        /// Follow up distance between vehicles.
        /// </summary>
        public static int FollowUpDistance => followUpDistance;
        /// <summary>
        /// Velocity multiplier.
        /// </summary>
        public static int VelocityMultiplier => velocityMultiplier;
        /// <summary>
        /// Intensity of breaking.
        /// </summary>
        public static int BreakPower => breakPower;
        /// <summary>
        /// Max velocity of the game.
        /// </summary>
        public static int MaxVelocity => maxVelocity;
        /// <summary>
        /// Main menu height.
        /// </summary>
        public static int MainMenuControlHeight => mainMenuControlHeight;
        /// <summary>
        /// Main menu width.
        /// </summary>
        public static int MainMenuControlWidth => mainMenuControlWidth;
        /// <summary>
        /// Height of the screen.
        /// </summary>
        public static int ScreenHeight => screenHeight;
        /// <summary>
        /// Width of the screen.
        /// </summary>
        public static int ScreenWidth => screenWidth;
        /// <summary>
        /// Width of a lane.
        /// </summary>
        public static int LaneWidth => laneWidth;
        /// <summary>
        /// Number of the lanes.
        /// </summary>
        public static int LanesNumber => lanesNumber;
    }
}
