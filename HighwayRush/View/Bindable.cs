﻿// <copyright file="Bindable.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.View
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Base class defining information for data binding.
    /// </summary>
    public abstract class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// PropertyChangedEventHandler for raising property changed events.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Method raising PropertyChanged event, receiving name of the property as parameter.
        /// </summary>
        /// <param name="name">Name of the property</param>
        protected void OnPropertyChanged(string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Method responsible for setting property data while raising PropertyChanged event.
        /// </summary>
        /// <typeparam name="T">Type of the property</typeparam>
        /// <param name="field">Field of the property</param>
        /// <param name="value">Value to set the field to</param>
        /// <param name="name">Name of the caller</param>
        protected void SetProperty<T>(ref T field, T value, [CallerMemberName] string name = null)
        {
            field = value;
            OnPropertyChanged(name);
        }
    }
}
