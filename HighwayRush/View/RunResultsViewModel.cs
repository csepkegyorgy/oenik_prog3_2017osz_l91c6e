﻿// <copyright file="RunResultsViewModel.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.View
{
    /// <summary>
    /// Viewmodel class defining information for its corresponding window
    /// </summary>
    public class RunResultsViewModel : Bindable
    {
        string playerName;
        string playerScore;
        string playerTime;

        /// <summary>
        /// The player's name
        /// </summary>
        public string PlayerName
        {
            get { return this.playerName; }
            set
            {
                SetProperty(ref playerName, value);
            }
        }

        /// <summary>
        /// The player's score
        /// </summary>
        public string PlayerScore
        {
            get { return this.playerScore; }
            set
            {
                SetProperty(ref playerScore, value);
            }
        }

        /// <summary>
        /// The time of the player's run
        /// </summary>
        public string PlayerTime
        {
            get { return this.playerTime; }
            set
            {
                SetProperty(ref playerTime, value);
            }
        }

        /// <summary>
        /// Base constructor for RunResultsViewModel class receiving run information as parameter
        /// </summary>
        /// <param name="playerName">Name of the player</param>
        /// <param name="score">Score of the player's run</param>
        /// <param name="time">Time of the player's run</param>
        public RunResultsViewModel(string playerName, int score, double time)
        {
            PlayerName = playerName;
            PlayerScore = score.ToString();
            PlayerTime = time.ToString();
            OnPropertyChanged();
        }

        /// <summary>
        /// Base constructor for RunResultsViewModel class
        /// </summary>
        public RunResultsViewModel()
        {

        }
    }
}
