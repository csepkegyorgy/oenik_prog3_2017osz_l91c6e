﻿// <copyright file="LaneEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.View
{
    using HighwayRush.Core;
    using HighwayRush.Core.Entities;
    using HighwayRush.Resources;
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// FrameworkElement control class defining information for the main menu's visual.
    /// </summary>
    public class MainMenuControl : FrameworkElement
    {
        MainMenuModel model;
        MainMenuLogic logic;
        Rect controlRectangle;
        DispatcherTimer timer;
        Pen p = new Pen(Brushes.Black, 1);
        
        /// <summary>
        /// mainmenucontrol constructor
        /// </summary>
        public MainMenuControl()
        {
            Loaded += MainMenuControl_Loaded;
        }

        private void MainMenuControl_Loaded(object sender, RoutedEventArgs e)
        {
            controlRectangle = new Rect(0, 0, Config.MainMenuControlWidth, Config.MainMenuControlHeight);
            model = new MainMenuModel();
            logic = new MainMenuLogic(model);
            model.PropertyChanged += Model_PropertyChanged;
            InvalidateVisual();

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                timer = new DispatcherTimer()
                {
                    Interval = TimeSpan.FromMilliseconds(1000 / Config.Fps)
                };
                timer.Tick += Timer_Tick; ;
                timer.Start();
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            logic.OneTick();
            InvalidateVisual();
        }

        private void Model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            InvalidateVisual();
        }

        /// <summary>
        /// on render method
        /// </summary>
        /// <param name="drawingContext">dc</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (model != null)
            {
                drawingContext.DrawRectangle(Config.LanesColor, null, controlRectangle);
                foreach (LaneSeparatorEntity laneSeparator in this.model.LaneSeparators)
                {
                    drawingContext.DrawGeometry(laneSeparator.Brush, p, laneSeparator.Area);
                }
                drawingContext.DrawGeometry(this.model.PlayerCar.Brush, null, this.model.PlayerCar.Area);
            }
        }
    }
}
