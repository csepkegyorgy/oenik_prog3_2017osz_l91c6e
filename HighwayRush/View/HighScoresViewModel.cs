﻿// <copyright file="HighScoresViewModel.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.View
{
    /// <summary>
    /// Viewmodel class defining information for its corresponding window
    /// </summary>
    public class HighScoresViewModel
    {
        string[] highScores;
        /// <summary>
        /// The reference for the high scores.
        /// </summary>
        public string[] HighScores { get => highScores; set => highScores = value; }

        /// <summary>
        /// Base constructor for HighScoresViewModel class
        /// </summary>
        public HighScoresViewModel()
        {

        }
    }
}
