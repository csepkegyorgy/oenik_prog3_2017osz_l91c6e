﻿// <copyright file="GameControl.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.View
{
    using HighwayRush.Core;
    using HighwayRush.Core.Entities;
    using HighwayRush.Core.Entities.BaseEntities;
    using HighwayRush.Resources;
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;
    using System.Diagnostics;

    /// <summary>
    /// FrameworkElement control class defining information for the game's runtime phase.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        GameModel model;
        GameLogic logic;
        DispatcherTimer timer;
        Pen p = new Pen(Brushes.Black, 1);
        Rect lanes = new Rect((Config.ScreenWidth - (Config.LanesNumber * Config.LaneWidth)) / 2, 0, Config.LanesNumber * Config.LaneWidth, Config.ScreenHeight);
        double time = 0;
        Stopwatch stopwatch;

        private void Model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            InvalidateVisual();
        }

        /// <summary>
        /// The method responsible for rendering game model information.
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (model != null)
            {
                drawingContext.DrawRectangle(Config.BackgroundColor, p, model.VisibleGameArea);

                drawingContext.DrawRectangle(Config.LanesColor, null, lanes);
                foreach (LaneSeparatorEntity laneSeparator in model.LaneSeparators)
                {
                    drawingContext.DrawGeometry(laneSeparator.Brush, p, laneSeparator.Area);
                }
                drawingContext.DrawGeometry(model.Cat.Brush, null, model.Cat.Area);
                drawingContext.DrawGeometry(model.Player.Brush, null, model.Player.Area);
                foreach (GuardrailEntity guardrail in model.Guardrails)
                {
                    drawingContext.DrawGeometry(guardrail.Brush, null, guardrail.Area);
                }
                foreach (VehicleEntity vehicle in model.Vehicles)
                {
                    drawingContext.DrawGeometry(vehicle.Brush, null, vehicle.Area);
                }
                foreach (BoostEntity boost in this.model.Boosts)
                {
                    drawingContext.DrawGeometry(boost.Brush, null, boost.Area);
                }

                drawingContext.DrawText(new FormattedText("Speed: " + model.Velocity / 10 + " km/h", System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 20, p.Brush), new Point(15, 15));
                drawingContext.DrawText(new FormattedText("Score: " + (int)model.Score, System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 20, p.Brush), new Point(15, 35));
                drawingContext.DrawText(new FormattedText("Time: " + model.Time + " seconds", System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 20, p.Brush), new Point(15, 55));
                drawingContext.DrawText(new FormattedText("Bonus Threshold: " + model.ScoreThreshold + " km/h", System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 20, p.Brush), new Point(15, 75));

                drawingContext.DrawGeometry(model.Bird.Brush, null, model.Bird.Area);

                if (model.StartState)
                    drawingContext.DrawText(new FormattedText(model.StartCounter.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 80, p.Brush), new Point(Config.ScreenWidth / 2 - 20, Config.ScreenHeight / 2 - 20));
            }
        }
        /// <summary>
        /// Base constructor for GameControl class
        /// </summary>
        public GameControl()
        {
            Loaded += GameControl_Loaded;
        }

        /// <summary>
        /// The method responsible for setting model and logic data receiving these as parameter 
        /// </summary>
        /// <param name="model">Game model of the game</param>
        /// <param name="logic">Game logic of the game</param>
        public void SetData(GameModel model, GameLogic logic)
        {
            this.model = model;
            this.logic = logic;

            InvalidateVisual();
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            model.PropertyChanged += Model_PropertyChanged;
            InvalidateVisual();

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                win.PreviewKeyDown += Win_PreviewKeyDown;
                win.PreviewKeyUp += Win_PreviewKeyUp;
                timer = new DispatcherTimer()
                {
                    Interval = TimeSpan.FromMilliseconds(1000 / Config.Fps)
                };
                timer.Tick += Timer_Tick;
                timer.Start();
                stopwatch = new Stopwatch();
                stopwatch.Start();
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            time += 1000 / Config.Fps;
            if(logic.OneTick(stopwatch))
            {
                timer.Stop();
                RunResultsViewModel rrvm = new RunResultsViewModel(this.model.PlayerName, (int)this.model.Score, this.model.Time);
                RunResultsWindow rrw = new RunResultsWindow(rrvm);
                rrw.ShowDialog();
                Window win = Window.GetWindow(this);
                win.Close();
            }
            
            InvalidateVisual();
        }

        private void Win_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case System.Windows.Input.Key.W: logic.Acceleration = false; break;
                case System.Windows.Input.Key.S: logic.Breaking = false; break;
                case System.Windows.Input.Key.A: logic.SteeringLeft = false; break;
                case System.Windows.Input.Key.D: logic.SteeringRight = false; break;
            }
            InvalidateVisual();
        }

        private void Win_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case System.Windows.Input.Key.W: logic.Acceleration = true; break;
                case System.Windows.Input.Key.S: logic.Breaking = true; break;
                case System.Windows.Input.Key.A: logic.SteeringLeft = true; break;
                case System.Windows.Input.Key.D: logic.SteeringRight = true; break;
            }
            InvalidateVisual();
        }
    }
}