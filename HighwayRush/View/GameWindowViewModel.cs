﻿// <copyright file="GameWindowViewModel.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.View
{
    using HighwayRush.Core.Entities;

    /// <summary>
    /// Viewmodel class defining information for its corresponding window
    /// </summary>
    public class GameWindowViewModel
    {
        /// <summary>
        /// The color of the player's car.
        /// </summary>
        public CarEntity.CarType CarColorType { get; set; }
    }
}
