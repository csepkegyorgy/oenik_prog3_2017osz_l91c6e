﻿using HighwayRush.Core.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighwayRush.View
{
    /// <summary>
    /// Viewmodel class defining information for its corresponding window
    /// </summary>
    public class MainWindowViewModel : Bindable
    {
        CarType carColorType;
        ObservableCollection<CarType> carColors;

        /// <summary>
        /// Type of the car's color
        /// </summary>
        public enum CarType
        {
            /// <summary>
            /// Red
            /// </summary>
            Red,
            /// <summary>
            /// Blue
            /// </summary>
            Blue,
            /// <summary>
            /// Yellow
            /// </summary>
            Yellow
        }

        /// <summary>
        /// The reference to CarColorType
        /// </summary>
        public CarType CarColorType
        {
            get
            {
                return this.carColorType;
            }
            set
            {
                SetProperty(ref carColorType, value);
            }
        }

        /// <summary>
        /// The reference to CarColors collection
        /// </summary>
        public ObservableCollection<CarType> CarColors { get => carColors; set => SetProperty(ref carColors, value); }

        /// <summary>
        /// Base constructor for MainWindowViewModel class
        /// </summary>
        public MainWindowViewModel()
        {
            CarColors = new ObservableCollection<CarType>
            {
                CarType.Blue,
                CarType.Red,
                CarType.Yellow
            };
        }
    }
}
