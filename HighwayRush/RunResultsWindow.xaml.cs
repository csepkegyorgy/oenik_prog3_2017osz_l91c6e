﻿// <copyright file="RunResultsWindow.xaml.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush
{
    using HighwayRush.View;
    using System.IO;
    using System.Windows;

    /// <summary>
    /// Interaction logic for RunResultsWindow.xaml
    /// This window shows the the results of the player's run, then saves it.
    /// </summary>
    public partial class RunResultsWindow : Window
    {
        RunResultsViewModel vm;

        /// <summary>
        /// Base constructor for RunResultsViewModel class receiving viewmodel as parameter
        /// </summary>
        /// <param name="vm">Viewmodel containing information about the player's run</param>
        public RunResultsWindow(RunResultsViewModel vm)
        {
            InitializeComponent();
            this.vm = vm;
            DataContext = vm;
        }

        private void MainMenu_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(vm.PlayerName))
            {
                string[] lines;
                try
                {
                    lines = File.ReadAllLines("highscores.txt");
                }
                catch(FileNotFoundException)
                {
                    lines = new string[0];
                }
                string newLine = vm.PlayerName + " " + vm.PlayerScore + " " + vm.PlayerTime;
                string[] newLines = new string[lines.Length + 1];
                for (int i = 0; i < lines.Length; i++)
                {
                    newLines[i] = lines[i];
                }
                newLines[lines.Length] = newLine;
                File.WriteAllLines("highscores.txt", newLines);

                MainWindow mw = new MainWindow();
                mw.Show();
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Enter your name first!");
            }
        }
    }
}
