﻿// <copyright file="GameLogic.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core
{
    using HighwayRush.Core.Entities;
    using HighwayRush.Core.Entities.BaseEntities;
    using HighwayRush.Resources;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;
    using System.Diagnostics;

    /// <summary>
    /// Logic class defining logic for the game's run phase.
    /// </summary>
    public class GameLogic
    {
        GameModel model;
        private static Random rand = new Random();

        /// <summary>
        /// Is player accelerating.
        /// </summary>
        public bool Acceleration { get; set; }
        /// <summary>
        /// Is player breaking.
        /// </summary>
        public bool Breaking { get; set; }
        /// <summary>
        /// Is player steering left.
        /// </summary>
        public bool SteeringLeft { get; set; }
        /// <summary>
        /// Is player steering right.
        /// </summary>
        public bool SteeringRight { get; set; }
        /// <summary>
        /// The reference to random number generation class.
        /// </summary>
        public static Random Rand { get => rand; set => rand = value; }

        double accelerator = 2;
        double steerRightFactor = 0;
        double steerLeftFactor = 0;
        int lastBoost = 0;
        double invulnerableFor = 0;

        Stopwatch steadyHighSpeedStopwatch;

        /// <summary>
        /// Base constructor for GameLogic class receiving model as parameter
        /// </summary>
        /// <param name="model">model of the game</param>
        public GameLogic(GameModel model)
        {
            this.model = model;
            steadyHighSpeedStopwatch = new Stopwatch();
        }

        /// <summary>
        /// Method responsible for handling one tick of the game, receiving a stopwatch as paramtere.
        /// </summary>
        /// <param name="sw">The stopwatch of the game</param>
        /// <returns></returns>
        public bool OneTick(Stopwatch sw)
        {
            StarterTick(sw);

            if (PlayerTick(sw))
                return true;

            LaneSeparatorsTick();
            GuardrailsTick();

            if(this.model.Velocity > Config.ThresholdVelocity)
            {
                if(this.model.VehiclesPattern.Count == 0)
                {
                    for (int i = 0; i < Rand.Next(10, 20); i++)
                    {
                        GeneratePatternGroup();
                    }
                }
                if (this.model.Vehicles.Count <= 0 && this.model.VehiclesPattern.Count > 0)
                    GenerateRoadEntitiesFromPatternGroup();
            }
            else
            {
                if(this.model.VehiclesPattern.Count > 0)
                    this.model.VehiclesPattern.Clear();
            }
            VehiclesTick();
            BirdTick(sw);
            BoostsTick(sw);
            CatTick(sw);

            return false;
        }

        private void GenerateRoadEntitiesFromPatternGroup()
        {
            int startX = 0;
            for (int i = this.model.VehiclesPattern.Count-1; i >=0 ; i--)
            {
                for (int j = 0; j < this.model.VehiclesPattern[i].Length; j++)
                {
                    if (this.model.VehiclesPattern[i][j] == true)
                    {
                        if(i > 0 && this.model.VehiclesPattern[i-1][j] == true)
                        {
                            if(i > 1 && this.model.VehiclesPattern[i-2][j] == true)
                            {
                                if(Rand.Next(0,2) == 1)
                                {
                                    this.model.VehiclesPattern[i - 1][j] = false;
                                    this.model.VehiclesPattern[i - 2][j] = false;
                                    startX = ((Config.ScreenWidth - (Config.LaneWidth * Config.LanesNumber)) / 2) + (Config.LaneWidth / 2) - (TruckEntity.TruckWidth / 2);
                                    this.model.Vehicles.Add(new TruckEntity(startX + (j * (Config.LaneWidth)), (-Config.FollowUpDistance*3 - TruckEntity.TruckHeight) - (i * (CarEntity.CarHeight + Config.FollowUpDistance))));
                                }
                            }
                            else
                            {
                                if(Rand.Next(0,2) == 1)
                                {
                                    this.model.VehiclesPattern[i - 1][j] = false;
                                    startX = ((Config.ScreenWidth - (Config.LaneWidth * Config.LanesNumber)) / 2) + (Config.LaneWidth / 2) - (BusEntity.BusWidth / 2);
                                    this.model.Vehicles.Add(new BusEntity(startX + (j * (Config.LaneWidth)), (-Config.FollowUpDistance*2 - BusEntity.BusHeight) - (i * (CarEntity.CarHeight + Config.FollowUpDistance))));
                                }
                            }
                        }
                        else
                        {
                            startX = ((Config.ScreenWidth - (Config.LaneWidth * Config.LanesNumber)) / 2)
                                + (Config.LaneWidth / 2) - (CarEntity.CarWidth / 2);

                            this.model.Vehicles.Add(new CarEntity(
                                startX + (j * (Config.LaneWidth)),
                                (-Config.FollowUpDistance - CarEntity.CarHeight) - (i * (CarEntity.CarHeight + Config.FollowUpDistance)),
                                (CarEntity.CarType)Rand.Next(0, 3)));
                        }
                    }
                }
            }
            this.model.VehiclesPattern.Clear();
        }

        private bool PlayerTick(Stopwatch sw)
        {
            if (!this.model.StartState)
            {
                if (model.Velocity - 15 >= 0)
                    model.Velocity -= 5;
                if (Acceleration)
                    Accelerate();
                if (Breaking)
                    Break(1);
                if (SteeringLeft || this.steerLeftFactor > 0)
                    SteerLeft();
                if (SteeringRight || this.steerRightFactor > 0)
                    SteerRight();

                if(invulnerableFor > 0)
                {
                    if (invulnerableFor - (Config.Fps / 1000) <= 0)
                    {
                        invulnerableFor = 0;
                        this.model.Player.CarColorType = this.model.OriginalCarType;
                    }
                    else
                    {
                        invulnerableFor -= (Config.Fps / 1000);
                    }
                }
                else
                {
                    foreach (VehicleEntity vehicle in this.model.Vehicles)
                    {
                        if (this.model.Player.IsCollidingWith(vehicle))
                        {
                            return true;
                        }
                    }
                }
                if (this.model.Player.IsDirectCollidingWith(this.model.Cat) && this.model.Cat.Frame != 3)
                {
                    this.model.Cat.Frame = 3;
                    this.model.Score -= 100;
                    this.model.Cat.VelocityX = 0;
                }
                List<BoostEntity> boostsToRemove = new List<BoostEntity>();
                foreach (BoostEntity boost in this.model.Boosts)
                {
                    if(this.model.Player.IsDirectCollidingWith(boost))
                    {
                        boostsToRemove.Add(boost);
                        ApplyBoost(boost);
                    }
                }
                foreach (BoostEntity boost in boostsToRemove)
                {
                    this.model.Boosts.Remove(boost);
                }

                this.model.Time = sw.Elapsed.Seconds - 3;
                if (this.model.Velocity > Config.ThresholdVelocity)
                {
                    if (!steadyHighSpeedStopwatch.IsRunning)
                        steadyHighSpeedStopwatch.Start();
                    this.model.Score += (this.model.Velocity / 100) / Config.Fps;
                    if (steadyHighSpeedStopwatch.Elapsed.Seconds != 0 && steadyHighSpeedStopwatch.Elapsed.Seconds % 10 == 0 && this.model.ScoreThreshold*10 <= this.model.Velocity)
                    {
                        if (this.model.ScoreThreshold < 100)
                            this.model.ScoreThreshold += 10;
                        steadyHighSpeedStopwatch.Reset();
                        this.model.Score += 100;
                    }
                }
                else
                {
                    if (this.model.Velocity < (this.model.ScoreThreshold * 10) / 2)
                    {
                        this.model.Score -= 0.25;
                    }
                    steadyHighSpeedStopwatch.Stop();
                }

                if(this.model.Velocity < 30)
                {
                    bool canRemove = true;
                    foreach (VehicleEntity vehicle in this.model.Vehicles)
                    {
                        if (vehicle.Area.Bounds.Y >= -TruckEntity.TruckHeight)
                            canRemove = false;
                    }
                    if (canRemove)
                    {
                        this.model.Vehicles.Clear();
                    }
                }
            }
            return false;
        }

        private void StarterTick(Stopwatch sw)
        {
            if (this.model.StartState)
            {
                if (sw.Elapsed.Seconds < 1)
                    this.model.StartCounter = 3;
                else if (sw.Elapsed.Seconds < 2)
                    this.model.StartCounter = 2;
                else if (sw.Elapsed.Seconds < 3)
                    this.model.StartCounter = 1;
                if (sw.Elapsed.Seconds > 3)
                    this.model.StartState = false;
            }
        }

        private void LaneSeparatorsTick()
        {
            foreach (LaneSeparatorEntity laneSeparator in model.LaneSeparators)
            {
                laneSeparator.Area = new RectangleGeometry(
                    new System.Windows.Rect(laneSeparator.Area.Bounds.X,
                        laneSeparator.Area.Bounds.Y + (double)model.Velocity / (double)Config.Fps,
                        LaneSeparatorEntity.LaneSeparatorWidth,
                        LaneSeparatorEntity.LaneSeparatorHeight));

                if (laneSeparator.Area.Bounds.Y > Config.ScreenHeight)
                {
                    laneSeparator.Area = new RectangleGeometry(
                        new System.Windows.Rect(laneSeparator.Area.Bounds.X,
                        laneSeparator.Area.Bounds.Y - Config.ScreenHeight - LaneSeparatorEntity.LaneSeparatorHeight*2,
                        LaneSeparatorEntity.LaneSeparatorWidth,
                        LaneSeparatorEntity.LaneSeparatorHeight));
                }
            }
        }

        private void GuardrailsTick()
        {
            foreach (GuardrailEntity guardrail in model.Guardrails)
            {
                guardrail.Area = new RectangleGeometry(
                    new System.Windows.Rect(guardrail.Area.Bounds.X,
                        guardrail.Area.Bounds.Y + (double)model.Velocity / (double)Config.Fps,
                        GuardrailEntity.GuardrailWidth,
                        GuardrailEntity.GuardrailHeight));

                if (guardrail.Area.Bounds.Y > Config.ScreenHeight)
                {
                    guardrail.Area = new RectangleGeometry(
                        new System.Windows.Rect(guardrail.Area.Bounds.X,
                        guardrail.Area.Bounds.Y - Config.ScreenHeight - GuardrailEntity.GuardrailHeight,
                        GuardrailEntity.GuardrailWidth,
                        GuardrailEntity.GuardrailHeight));
                }
            }
        }

        private void VehiclesTick()
        {
            List<VehicleEntity> vehiclesToRemove = new List<VehicleEntity>();
            foreach (VehicleEntity vehicle in this.model.Vehicles)
            {
                vehicle.Area = new RectangleGeometry(
                    new System.Windows.Rect(vehicle.Area.Bounds.X,
                        vehicle.Area.Bounds.Y + (double)model.Velocity / (double)Config.Fps - Config.VehiclesVelocity / Config.Fps,
                        vehicle.VehicleWidth,
                        vehicle.VehicleHeight));

                if (vehicle.Area.Bounds.Y > Config.ScreenHeight)
                {
                    vehiclesToRemove.Add(vehicle);
                }
            }
            foreach (VehicleEntity vehicle in vehiclesToRemove)
            {
                this.model.Vehicles.Remove(vehicle);
            }
        }

        private void BirdTick(Stopwatch sw)
        {
            if (this.model.Bird.Active)
            {
                this.model.Bird.Area = new RectangleGeometry(new Rect(
                    this.model.Bird.Area.Bounds.X - (BirdEntity.VelocityX / Config.Fps),
                    this.model.Bird.Area.Bounds.Y - (BirdEntity.VelocityY / Config.Fps) + (model.Velocity / Config.Fps),
                    BirdEntity.BirdWidth,
                    BirdEntity.BirdHeight));

                if (this.model.Bird.Area.Bounds.X < 0 || this.model.Bird.Area.Bounds.Y > Config.ScreenHeight)
                {
                    this.model.Bird.Area = new RectangleGeometry(new Rect(
                        Config.ScreenWidth,
                        0,
                        BirdEntity.BirdWidth,
                        BirdEntity.BirdHeight));
                    this.model.Bird.BirdColorType = (BirdEntity.BirdType)Rand.Next(0, 2);
                    this.model.Bird.Active = false;
                }
            }
            if (sw.Elapsed.Seconds % 20 == 0)
            {
                this.model.Bird.Active = true;
            }
        }

        private void CatTick(Stopwatch sw)
        {
            if (this.model.Cat.Active)
            {
                this.model.Cat.Area = new RectangleGeometry(new Rect(
                    this.model.Cat.Area.Bounds.X - (this.model.Cat.VelocityX / Config.Fps),
                    this.model.Cat.Area.Bounds.Y + model.Velocity / Config.Fps,
                    CatEntity.CatWidth,
                    CatEntity.CatHeight));
                if(this.model.Cat.Frame != 3)
                {
                    this.model.Cat.Frame++;
                    if (this.model.Cat.Frame == 3)
                    {
                        this.model.Cat.Frame = 0;
                    }
                }

                if (this.model.Cat.Area.Bounds.X < ((Config.ScreenWidth - (Config.LaneWidth * Config.LanesNumber)) / 2) - GuardrailEntity.GuardrailWidth || this.model.Cat.Area.Bounds.Y > Config.ScreenHeight)
                {
                    this.model.Cat.Area = new RectangleGeometry(new Rect(
                        Config.ScreenWidth - ((Config.ScreenWidth - (Config.LaneWidth * Config.LanesNumber)) / 2),
                        -CatEntity.CatHeight,
                        CatEntity.CatWidth,
                        CatEntity.CatHeight));
                    this.model.Cat.Active = false;
                }
            }
            if (sw.Elapsed.Seconds % 15 == 0)
            {
                this.model.Cat.Active = true;
            }
        }

        private void BoostsTick(Stopwatch sw)
        {
            if(sw.Elapsed.Seconds != 0 && sw.Elapsed.Seconds % 10 == 0)
            {
                if(sw.Elapsed.Seconds != lastBoost)
                {
                    BoostEntity.BoostType randomType = BoostEntity.BoostType.Score;
                    if (Rand.Next(0, 5) == 0)
                        randomType = BoostEntity.BoostType.Shield;
                    lastBoost = sw.Elapsed.Seconds;
                    int startX = ((Config.ScreenWidth - (Config.LaneWidth * Config.LanesNumber)) / 2) + (Config.LaneWidth / 2) - (BoostEntity.BoostWidth / 2);
                    this.model.Boosts.Add(new BoostEntity(
                        startX + Rand.Next(0, 5) * Config.LaneWidth,
                        -BoostEntity.BoostHeight,
                        randomType
                    ));
                }
            }

            List<BoostEntity> boostsToRemove = new List<BoostEntity>();
            foreach (BoostEntity boost in model.Boosts)
            {
                boost.Area = new RectangleGeometry(
                    new System.Windows.Rect(boost.Area.Bounds.X,
                        boost.Area.Bounds.Y + (double)BoostEntity.VelocityY / (double)Config.Fps,
                        BoostEntity.BoostWidth,
                        BoostEntity.BoostHeight));

                if (boost.Area.Bounds.Y > Config.ScreenHeight)
                {
                    boostsToRemove.Add(boost);
                }
            }
            foreach (BoostEntity boost in boostsToRemove)
            {
                model.Boosts.Remove(boost);
            }
        }

        private void ApplyBoost(BoostEntity boost)
        {
            if(boost.TypeOfBoost == BoostEntity.BoostType.Score)
            {
                this.model.Score += 50;
            }
            else if(boost.TypeOfBoost == BoostEntity.BoostType.Shield)
            {
                invulnerableFor = 8;
                this.model.Player.CarColorType = CarEntity.CarType.Invulnerable;
            }
        }

        private void GeneratePatternGroup()
        {
            bool[] inputRow;
            if(this.model.VehiclesPattern.Count == 0)
            {
                inputRow = new bool[5];
                int fills = Rand.Next(0, 4);
                for (int i = 0; i < fills; i++)
                {
                    bool ok = false;
                    do
                    {
                        int rand = Rand.Next(0, Config.LanesNumber);
                        if(inputRow[rand] == false)
                        {
                            inputRow[rand] = true;
                            ok = true;
                        }
                    } while (!ok);
                }
                this.model.VehiclesPattern.Add(inputRow);
            }
            else
            {
                inputRow = this.model.VehiclesPattern.Last();
            }

            bool[] newRowOne = new bool[Config.LanesNumber];
            bool[] newRowTwo = new bool[Config.LanesNumber];

            int c = Config.LanesNumber;
            for (int i = 0; i < inputRow.Length; i++)
            {
                if (inputRow[i] == true)
                    c--;
            }

            bool validPatternGroup = false;
            do
            {
                for (int i = 0; i < newRowOne.Length; i++)
                {
                    newRowOne[i] = false;
                    newRowTwo[i] = false;
                }

                for (int i = 0; i < c; i++)
                {
                    bool ok = false;
                    do
                    {
                        int randomNumber = Rand.Next(0, Config.LanesNumber);
                        if (Rand.Next(0, 2) == 0)
                        {
                            if(newRowOne[randomNumber] == false)
                            {
                                newRowOne[randomNumber] = true;
                                ok = true;
                            }
                        }
                        else
                        {
                            if (newRowTwo[randomNumber] == false)
                            {
                                newRowTwo[randomNumber] = true;
                                ok = true;
                            }
                        }
                    } while (!ok);
                }
                validPatternGroup = ValidatePatternGroup( inputRow, newRowOne, newRowTwo);
            } while (!validPatternGroup);

            this.model.VehiclesPattern.Add(newRowOne);
            this.model.VehiclesPattern.Add(newRowTwo);
        }

        private bool ValidatePatternGroup(bool[] inputRow, bool[] newRowOne, bool[] newRowTwo)
        {
            List<int> startPoints = new List<int>();
            for (int i = 0; i < inputRow.Length; i++)
            {
                if(inputRow[i] == false)
                {
                    startPoints.Add(i);
                }
            }
            bool[] IsValidStartPoint = new bool[startPoints.Count];
            int j = 0;
            foreach (int startPoint in startPoints)
            {
                IsValidStartPoint[j] = IsValidPoint(startPoint, 0, inputRow, newRowOne, newRowTwo);
                j++;
            }
            foreach (bool validPoint in IsValidStartPoint)
            {
                if (validPoint == false)
                    return false;
            }
            return true;
        }

        bool IsValidPoint(int point, int level, bool[] inputRow, bool[] newRowOne, bool[] newRowTwo)
        {
            if(level == 0)
            {
                if(newRowOne[point] == false)
                {
                    return IsValidPoint(point, 1, inputRow, newRowOne, newRowTwo);
                }
                else if (point != 0 && point != (inputRow.Length - 1))
                {
                    if (inputRow[point - 1] == false)
                    {
                        if(newRowOne[point - 1] == false)
                            if(IsValidPoint(point - 1, 1, inputRow, newRowOne, newRowTwo))
                                return true;
                    }
                    else if (inputRow[point + 1] == false)
                    {
                        if (newRowOne[point + 1] == false)
                            if (IsValidPoint(point + 1, 1, inputRow, newRowOne, newRowTwo))
                                return true;
                    }
                    return false;
                }
                else if(point == 0)
                {
                    if (inputRow[point + 1] == false)
                    {
                        if (newRowOne[point + 1] == false)
                            if (IsValidPoint(point + 1, 1, inputRow, newRowOne, newRowTwo))
                                return true;
                    }
                }
                else
                {
                    if (inputRow[point - 1] == false)
                    {
                        if (newRowOne[point - 1] == false)
                            if (IsValidPoint(point - 1, 1, inputRow, newRowOne, newRowTwo))
                                return true;
                    }
                }
                return false;
            }
            else
            {
                if (newRowTwo[point] == false)
                {
                    return true;
                }
                else if (point != 0 && point != (inputRow.Length - 1))
                {
                    if (newRowOne[point - 1] == false)
                    {
                        if (newRowTwo[point - 1] == false)
                            return true;
                    }
                    else if (inputRow[point + 1] == false)
                    {
                        if (newRowTwo[point + 1] == false)
                            return true;
                    }
                    return false;
                }
                else if (point == 0)
                {
                    if (newRowOne[point + 1] == false)
                    {
                        if (newRowTwo[point + 1] == false)
                           return true;
                    }
                }
                else
                {
                    if (newRowOne[point - 1] == false)
                    {
                        if (newRowTwo[point - 1] == false)
                            return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Method responsible for acceleration of the player.
        /// </summary>
        protected void Accelerate()
        {
            if (accelerator <= 10)
                accelerator += 3;
            if (model.Velocity + accelerator > Config.MaxVelocity )
                model.Velocity = Config.MaxVelocity;
            else
                model.Velocity += accelerator;
        }
        /// <summary>
        /// Method responsible for breaking of the player.
        /// </summary>
        /// <param name="breakMultiplier"></param>
        protected void Break(int breakMultiplier)
        {
            if (model.Velocity - Config.BreakPower*breakMultiplier > 0)
                model.Velocity -= Config.BreakPower*breakMultiplier;
            else
                model.Velocity = 0;
        }

        private void SteerLeft()
        {
            double playerX = this.model.Player.Area.Bounds.X;
            double leftGuardrailX = ((Config.ScreenWidth - (Config.LanesNumber * Config.LaneWidth)) / 2) - (GuardrailEntity.GuardrailWidth / 2);

            if (SteeringLeft && this.steerLeftFactor == 0)
            {
                this.steerLeftFactor = 1;
            }
            else
            {
                this.steerLeftFactor = this.steerLeftFactor * 1.2;
                if (this.steerLeftFactor > 15)
                    this.steerLeftFactor = 10;

                if (!(playerX - steerLeftFactor <= leftGuardrailX))
                {
                    this.model.Player.SetPosition(playerX - steerLeftFactor,
                       model.Player.Area.Bounds.Y,
                       CarEntity.CarWidth,
                       CarEntity.CarHeight);
                }
                else
                {
                    this.model.Player.SetPosition(leftGuardrailX + 2,
                       model.Player.Area.Bounds.Y,
                       CarEntity.CarWidth,
                       CarEntity.CarHeight);
                    steerLeftFactor = 0;
                }
                if (playerX <= leftGuardrailX + (GuardrailEntity.GuardrailWidth / 2))
                {
                    this.model.Score -= 5;
                    Break(6);
                }
            }
            if(!SteeringLeft && this.steerLeftFactor > 0)
            {
                this.steerLeftFactor *= 0.75;
                if (this.steerLeftFactor < 0.5)
                    this.steerLeftFactor = 0;
            }
        }

        private void SteerRight()
        {
            double playerX = this.model.Player.Area.Bounds.X;
            double rightGuardrailX = (Config.ScreenWidth - ((Config.ScreenWidth - (Config.LanesNumber * Config.LaneWidth)) / 2)) + (GuardrailEntity.GuardrailWidth / 2);

            if (SteeringRight && this.steerRightFactor == 0)
            {
                this.steerRightFactor = 1;
            }
            else
            {
                this.steerRightFactor = this.steerRightFactor * 1.2;
                if (this.steerRightFactor > 15)
                    this.steerRightFactor = 10;

                if (!(playerX + CarEntity.CarWidth + steerRightFactor >= rightGuardrailX))
                {
                    this.model.Player.SetPosition(playerX + steerRightFactor,
                    model.Player.Area.Bounds.Y,
                    CarEntity.CarWidth,
                    CarEntity.CarHeight);
                }
                else
                {
                    this.model.Player.SetPosition(rightGuardrailX - 2 - CarEntity.CarWidth,
                       model.Player.Area.Bounds.Y,
                       CarEntity.CarWidth,
                       CarEntity.CarHeight);
                    steerRightFactor = 0;
                }

                if (playerX + CarEntity.CarWidth >= rightGuardrailX - (GuardrailEntity.GuardrailWidth / 2))
                {
                    this.model.Score -= 5;
                    Break(6);
                }
            }
            if (!SteeringRight && this.steerRightFactor > 0)
            {
                this.steerRightFactor *= 0.75;
                if (this.steerRightFactor < 0.5)
                    this.steerRightFactor = 0;
            }
        }
    }
}
