﻿// <copyright file="GameModel.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities
{
    using HighwayRush.Core.Entities.BaseEntities;
    using HighwayRush.Resources;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Model class defining data for the game's runtime phase.
    /// </summary>
    public class GameModel : INotifyPropertyChanged
    {
        CarEntity player;
        List<VehicleEntity> vehicles;
        List<LaneSeparatorEntity> laneSeparators;
        List<GuardrailEntity> guardrails;
        List<bool[]> vehiclesPattern;
        List<BoostEntity> boosts;
        BirdEntity bird;
        CatEntity cat;
        Rect visibleGameArea = new Rect(0, 0, Config.ScreenWidth, Config.ScreenHeight);
        double velocity = 0;
        double score = 0;

        /// <summary>
        /// Property changed event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Base constructor for GameModel class receiving game information as parameter
        /// </summary>
        /// <param name="type">Selected type of the player's car</param>
        public GameModel(CarEntity.CarType type)
        {
            OriginalCarType = type;
            this.player = new CarEntity((Config.ScreenWidth / 2) - (CarEntity.CarWidth / 2), Config.ScreenHeight - CarEntity.CarHeight, OriginalCarType);
            this.vehicles = new List<VehicleEntity>();
            this.laneSeparators = new List<LaneSeparatorEntity>();
            this.guardrails = new List<GuardrailEntity>();
            this.vehiclesPattern = new List<bool[]>();
            this.boosts = new List<BoostEntity>();
            this.StartState = true;
            this.ScoreThreshold = 50;
            this.bird = new BirdEntity(Config.ScreenWidth, 0, BirdEntity.BirdType.Dark);
            this.cat = new CatEntity(Config.ScreenWidth, 0);
            Velocity = 450;

            int startX = ((Config.ScreenWidth - (Config.LanesNumber * Config.LaneWidth)) / 2) - (LaneSeparatorEntity.LaneSeparatorWidth / 2);
            for (int i = 1; i < Config.LanesNumber; i++)
            {
                for (int j = -1; j < (Config.ScreenHeight / (LaneSeparatorEntity.LaneSeparatorHeight*2)) + 1; j++)
                {
                    laneSeparators.Add(new LaneSeparatorEntity(new RectangleGeometry(new Rect(
                        startX + (i * Config.LaneWidth),
                        j*LaneSeparatorEntity.LaneSeparatorHeight*2,
                        LaneSeparatorEntity.LaneSeparatorWidth,
                        LaneSeparatorEntity.LaneSeparatorHeight
                        ))));
                }
            }
            for (int i = -1; i < (Config.ScreenHeight / (GuardrailEntity.GuardrailHeight)) + 1; i++)
            {
                Guardrails.Add(new GuardrailEntity(new RectangleGeometry(new Rect(
                    ((Config.ScreenWidth - (Config.LanesNumber * Config.LaneWidth)) / 2) - (GuardrailEntity.GuardrailWidth),
                    i * GuardrailEntity.GuardrailHeight,
                    GuardrailEntity.GuardrailWidth,
                    GuardrailEntity.GuardrailHeight
                    ))));
            }
            for (int i = -1; i < (Config.ScreenHeight / (GuardrailEntity.GuardrailHeight)) + 1; i++)
            {
                Guardrails.Add(new GuardrailEntity(new RectangleGeometry(new Rect(
                    (Config.ScreenWidth - ((Config.ScreenWidth - (Config.LanesNumber * Config.LaneWidth)) / 2)),
                    i * GuardrailEntity.GuardrailHeight,
                    GuardrailEntity.GuardrailWidth,
                    GuardrailEntity.GuardrailHeight
                    ))));
            }
        }

        /// <summary>
        /// The reference to the Player's car.
        /// </summary>
        public CarEntity Player { get => player; }

        /// <summary>
        /// The reference to the Vehicles collection.
        /// </summary>
        public List<VehicleEntity> Vehicles { get => vehicles; }

        /// <summary>
        /// The reference to the LaneSeparator collection.
        /// </summary>
        public List<LaneSeparatorEntity> LaneSeparators { get => laneSeparators; }

        /// <summary>
        /// The reference to the Guardrail collection.
        /// </summary>
        public List<GuardrailEntity> Guardrails { get => guardrails; }

        /// <summary>
        /// The reference to the bird.
        /// </summary>
        public BirdEntity Bird { get => bird; set => bird = value; }

        /// <summary>
        /// The reference to the cat.
        /// </summary>
        public CatEntity Cat { get => cat; set => cat = value; }

        /// <summary>
        /// The reference to the Guardrail collection.
        /// </summary>
        public List<BoostEntity> Boosts { get => boosts; }

        /// <summary>
        /// The reference to the Guardrail collection.
        /// </summary>
        public List<bool[]> VehiclesPattern { get => vehiclesPattern; }

        /// <summary>
        /// The player's name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// The player's car type for the run.
        /// </summary>
        public CarEntity.CarType OriginalCarType { get; set; }

        /// <summary>
        /// The score that the player has achieved.
        /// </summary>
        public double Score
        {
            get
            {
                return this.score;
            }
            set
            {
                if (value < 0)
                {
                    this.score = 0;
                }
                else
                {
                    this.score = value;
                }
            }
        }

        /// <summary>
        /// The time (in seconds) of the Player's run. 
        /// </summary>
        public double Time { get; set; }

        /// <summary>
        /// The starting counter at the beginning.
        /// </summary>
        public int StartCounter { get; set; }

        /// <summary>
        /// Set to false after the player's run has started (after the counter).
        /// </summary>
        public bool StartState { get; set; }

        /// <summary>
        /// The speed threshold for score gaining.
        /// </summary>
        public int ScoreThreshold { get; set; }

        /// <summary>
        /// The player's (and the game's) momentary speed;
        /// </summary>
        public double Velocity
        {
            get
            {
                return velocity;
            }
            set
            {
                velocity = value;
            }
        }

        /// <summary>
        /// The reference to the game's visible area rectangle.
        /// </summary>
        public Rect VisibleGameArea { get => visibleGameArea; }
    }
}
