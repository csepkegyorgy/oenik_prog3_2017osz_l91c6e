﻿// <copyright file="MainMenuLogic.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core
{
    using HighwayRush.Core.Entities;
    using HighwayRush.Resources;
    using System.Windows.Media;

    /// <summary>
    /// Logic class defining logic for the main menu.
    /// </summary>
    public class MainMenuLogic
    {
        MainMenuModel model;

        /// <summary>
        /// Base constructor for MainMenuLogic class receiving model as parameter
        /// </summary>
        /// <param name="model">The model of the main menu</param>
        public MainMenuLogic(MainMenuModel model)
        {
            this.model = model;
        }

        /// <summary>
        /// One tick of the main menu.
        /// </summary>
        public void OneTick()
        {
            LaneSeparatorsTick();
        }

        private void LaneSeparatorsTick()
        {
            foreach (LaneSeparatorEntity laneSeparator in model.LaneSeparators)
            {
                laneSeparator.Area = new RectangleGeometry(
                    new System.Windows.Rect(laneSeparator.Area.Bounds.X,
                        laneSeparator.Area.Bounds.Y + (double)model.Velocity / (double)Config.Fps,
                        LaneSeparatorEntity.LaneSeparatorWidth,
                        LaneSeparatorEntity.LaneSeparatorHeight));

                if (laneSeparator.Area.Bounds.Y > Config.MainMenuControlHeight)
                {
                    laneSeparator.Area = new RectangleGeometry(
                        new System.Windows.Rect(laneSeparator.Area.Bounds.X,
                        laneSeparator.Area.Bounds.Y - Config.MainMenuControlHeight - LaneSeparatorEntity.LaneSeparatorHeight * 2,
                        LaneSeparatorEntity.LaneSeparatorWidth,
                        LaneSeparatorEntity.LaneSeparatorHeight));
                }
            }
        }
    }
}
