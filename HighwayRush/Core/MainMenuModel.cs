﻿// <copyright file="MainMenuModel.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core
{
    using HighwayRush.Core.Entities;
    using HighwayRush.Resources;
    using HighwayRush.View;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Model class defining date for the main menu's phase.
    /// </summary>
    public class MainMenuModel : Bindable
    {
        CarEntity playerCar;
        List<LaneSeparatorEntity> laneSeparators;
        int velocity;

        /// <summary>
        /// Base constructor for MainMenuModel class
        /// </summary>
        public MainMenuModel()
        {
            this.laneSeparators = new List<LaneSeparatorEntity>();
            this.playerCar = new CarEntity((Config.MainMenuControlWidth / 2) - (CarEntity.CarWidth / 2), (Config.MainMenuControlHeight / 2) - (CarEntity.CarHeight / 2), CarEntity.CarType.Red);
            velocity = 800;

            int startX = (Config.MainMenuControlWidth / 5) - (LaneSeparatorEntity.LaneSeparatorWidth / 2);
            for (int i = 0; i < Config.LanesNumber-1; i++)
            {
                for (int j = 0; j < (Config.MainMenuControlHeight / (LaneSeparatorEntity.LaneSeparatorHeight * 2)) + 1; j++)
                {
                    laneSeparators.Add(new LaneSeparatorEntity(new RectangleGeometry(new Rect(
                        startX + (i * (Config.MainMenuControlWidth / 5)),
                        j * LaneSeparatorEntity.LaneSeparatorHeight * 2,
                        LaneSeparatorEntity.LaneSeparatorWidth,
                        LaneSeparatorEntity.LaneSeparatorHeight
                        ))));
                }
            }
        }

        /// <summary>
        /// The velocity of the game.
        /// </summary>
        public int Velocity { get => velocity; set => velocity = value; }

        /// <summary>
        /// The reference to the player's car.
        /// </summary>
        public CarEntity PlayerCar { get => playerCar; }

        /// <summary>
        /// The reference to the lane separators' collection.
        /// </summary>
        public List<LaneSeparatorEntity> LaneSeparators { get => laneSeparators; }
    }
}
