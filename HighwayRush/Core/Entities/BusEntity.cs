﻿// <copyright file="BusEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities
{
    using HighwayRush.Core.Entities.BaseEntities;
    using System.Windows.Media;

    /// <summary>
    /// Entity class defining information about a bus vehicle of the traffic.
    /// </summary>
    public class BusEntity : VehicleEntity
    {
        /// <summary>
        /// The width of the bus.
        /// </summary>
        public const int BusWidth = 60;
        /// <summary>
        /// The height of the bus.
        /// </summary>
        public const int BusHeight = 200;

        /// <summary>
        /// Base constructor for BusEntity class receiving position as parameter.
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        public BusEntity(int x, int y)
            : base(new RectangleGeometry(new System.Windows.Rect(x, y, BusEntity.BusWidth, BusEntity.BusHeight)))
        {
        }

        /// <summary>
        /// Gets the ImageBrush of the bus.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return new ImageBrush(GetBitmapImageByName("bus.png"));
            }
        }

        /// <summary>
        /// Height of the bus.
        /// </summary>
        public override double VehicleHeight
        {
            get
            {
                return BusHeight;
            }
        }

        /// <summary>
        /// Width of the bus.
        /// </summary>
        public override double VehicleWidth
        {
            get
            {
                return BusWidth;
            }
        }
    }
}
