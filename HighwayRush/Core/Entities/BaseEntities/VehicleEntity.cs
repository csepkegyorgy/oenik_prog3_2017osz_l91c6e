﻿// <copyright file="VehicleEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities.BaseEntities
{
    using System.Windows.Media;

    /// <summary>
    /// Base class defining information about a vehicle of the game.
    /// </summary>
    abstract public class VehicleEntity : GameEntity
    {
        /// <summary>
        /// Base constructor for VehicleEntity class receiving area as parameter
        /// </summary>
        /// <param name="area">The area of the vehicle</param>
        protected VehicleEntity(Geometry area)
            :base(area)
        {
        }

        /// <summary>
        /// Sets the position of the entity.
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        /// <param name="w">width</param>
        /// <param name="h">height</param>
        public void SetPosition(double x, double y, double w, double h)
        {
            this.Area = new RectangleGeometry(new System.Windows.Rect(x, y, w, h));
        }

        /// <summary>
        /// Height of the vehicle.
        /// </summary>
        public abstract double VehicleHeight { get; }

        /// <summary>
        /// Width of the vehicle.
        /// </summary>
        public abstract double VehicleWidth { get; }
    }
}
