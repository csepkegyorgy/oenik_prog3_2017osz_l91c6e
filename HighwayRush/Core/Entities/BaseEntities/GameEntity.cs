﻿// <copyright file="GameEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities.BaseEntities
{
    using HighwayRush.Resources;
    using System;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Base class defining information about a GameEntity of the game.
    /// </summary>
    public abstract class GameEntity
    {
        private Geometry area;

        /// <summary>
        /// Base constructor for GameEntity class receiving area as parameter
        /// </summary>
        /// <param name="area">The area of the entity.</param>
        protected GameEntity(Geometry area)
        {
            this.area = area;
        }
        
        /// <summary>
        /// The reference to the area of the entity.
        /// </summary>
        public Geometry Area
        {
            get
            {
                return this.area;
            }
            set
            {
                area = value;
            }
        }

        /// <summary>
        /// Checks if this game entity is colliding with the given one.
        /// </summary>
        /// <param name="other">Game entity to check collision with</param>
        /// <returns></returns>
        public bool IsCollidingWith(GameEntity other)
        {
            if((this.Area.Bounds.TopLeft.Y + 10 < other.Area.Bounds.BottomLeft.Y))
            {
                return Geometry.Combine(this.Area, other.Area, GeometryCombineMode.Intersect, null).GetArea() > 0 + Config.SidewayCollisionThreshold;
            }
            else
            {
                return Geometry.Combine(this.Area, other.Area, GeometryCombineMode.Intersect, null).GetArea() > 0 + Config.FrontalCollisionThreshold;
            }
        }

        /// <summary>
        /// Checks if this game entity is colliding with the given one without any threshold applied.
        /// </summary>
        /// <param name="other">Game entity to check collision with</param>
        /// <returns></returns>
        public bool IsDirectCollidingWith(GameEntity other)
        {
            return Geometry.Combine(this.Area, other.Area, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }

        /// <summary>
        /// Gets the BitmapImage of a file from Resources/Image.
        /// </summary>
        /// <param name="resourceName">Name and extension of an existing picture in Resources/Image</param>
        /// <returns></returns>
        protected BitmapImage GetBitmapImageByName(string resourceName)
        {
            return new BitmapImage(new Uri($"pack://application:,,,/Resources/Image/{resourceName}"));
        }
        /// <summary>
        /// The brush of the entity.
        /// </summary>
        public abstract Brush Brush { get; }
    }
}
