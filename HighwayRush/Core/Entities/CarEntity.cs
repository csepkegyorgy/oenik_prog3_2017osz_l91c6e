﻿// <copyright file="CarEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities
{
    using HighwayRush.Core.Entities.BaseEntities;
    using System.Windows.Media;
    using System.Collections.Generic;

    /// <summary>
    /// Entity class defining information about a car vehicle of the traffic (and also the player).
    /// </summary>
    public class CarEntity : VehicleEntity
    {
        /// <summary>
        /// The width of the car.
        /// </summary>
        public const int CarWidth = 50;
        /// <summary>
        /// The height of the car.
        /// </summary>
        public const int CarHeight = 100;

        /// <summary>
        /// The color type of the car.
        /// </summary>
        public enum CarType
        {
            /// <summary>
            /// Red
            /// </summary>
            Red,
            /// <summary>
            /// Blue
            /// </summary>
            Blue,
            /// <summary>
            /// Yellow
            /// </summary>
            Yellow,
            /// <summary>
            /// invulnerable / transparent
            /// </summary>
            Invulnerable
        }

        private static readonly Dictionary<CarType, string> carTypeFileNames = new Dictionary<CarType, string>()
        {
            { CarType.Red, "car_red.png" },
            { CarType.Yellow, "car_yellow.png" },
            { CarType.Invulnerable, "car_invulnerable.png" },
            { CarType.Blue, "car_blue.png" }
        };

        /// <summary>
        /// The color type of the car.
        /// </summary>
        public CarType CarColorType { get; set; }

        /// <summary>
        /// Base constructor for CarEntity class receiving position and type as parameter
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        /// <param name="type">Color of the car</param>
        public CarEntity(int x, int y, CarType type)
            : base(new RectangleGeometry(new System.Windows.Rect(x, y, CarEntity.CarWidth, CarEntity.CarHeight)))
        {
            this.CarColorType = type;
        }

        /// <summary>
        /// Gets the ImageBrush of the car.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return new ImageBrush(GetBitmapImageByName(carTypeFileNames[CarColorType]));
            }
        }

        /// <summary>
        /// Height of the car.
        /// </summary>
        public override double VehicleHeight
        {
            get
            {
                return CarHeight;
            }
        }

        /// <summary>
        /// Width of the car.
        /// </summary>
        public override double VehicleWidth
        {
            get
            {
                return CarWidth;
            }
        }
    }
}
