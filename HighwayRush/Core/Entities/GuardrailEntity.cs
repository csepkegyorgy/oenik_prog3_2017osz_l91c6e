﻿// <copyright file="GuardrailEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities
{
    using HighwayRush.Core.Entities.BaseEntities;
    using System.Windows.Media;

    /// <summary>
    /// Entity class defining information about a guardrail on the side of the road.
    /// </summary>
    public class GuardrailEntity : GameEntity
    {
        /// <summary>
        /// Width of a guardrail.
        /// </summary>
        public const int GuardrailWidth = 12;
        /// <summary>
        /// Height of a guardrail.
        /// </summary>
        public const int GuardrailHeight = 45;
        private const string BrushFileName = "guardrail.png";

        /// <summary>
        /// Base constructor for GuardRailEntity class receiving area as parameter
        /// </summary>
        /// <param name="area">Area of a guardrail</param>
        public GuardrailEntity(Geometry area) : base(area)
        {
        }

        /// <summary>
        /// Gets the ImageBrush of the guardrail.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return new ImageBrush(GetBitmapImageByName(BrushFileName));
            }
        }
    }
}
