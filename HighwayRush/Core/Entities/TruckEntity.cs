﻿// <copyright file="TruckEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities
{
    using HighwayRush.Core.Entities.BaseEntities;
    using System.Windows.Media;

    /// <summary>
    /// Entity class defining information about a truck vehicle of the traffic.
    /// </summary>
    public class TruckEntity : VehicleEntity
    {
        /// <summary>
        /// The width of the truck.
        /// </summary>
        public const int TruckWidth = 70;
        /// <summary>
        /// The height of the truck.
        /// </summary>
        public const int TruckHeight = 300;

        /// <summary>
        /// Base constructor for TruckEntity class receiving position as parameter
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        public TruckEntity(int x, int y)
            : base(new RectangleGeometry(new System.Windows.Rect(x, y, TruckEntity.TruckWidth, TruckEntity.TruckHeight)))
        {
        }

        /// <summary>
        /// Gets the ImageBrush of the truck.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return new ImageBrush(GetBitmapImageByName("truck.png"));
            }
        }

        /// <summary>
        /// Height of the truck.
        /// </summary>
        public override double VehicleHeight
        {
            get
            {
                return TruckHeight;
            }
        }

        /// <summary>
        /// Width of the truck.
        /// </summary>
        public override double VehicleWidth
        {
            get
            {
                return TruckWidth;
            }
        }
    }
}
