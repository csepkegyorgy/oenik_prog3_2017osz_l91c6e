﻿// <copyright file="CatEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities
{
    using HighwayRush.Core.Entities.BaseEntities;
    using System.Collections.Generic;
    using System.Windows.Media;

    /// <summary>
    /// Entity class defining information about a cat wandering on the road.
    /// </summary>
    public class CatEntity : GameEntity
    {
        /// <summary>
        /// Width of the cat.
        /// </summary>
        public const int CatWidth = 18;
        /// <summary>
        /// Height of the cat.
        /// </summary>
        public const int CatHeight = 14;

        bool active;
        int frame = 0;

        private static readonly Dictionary<int, string> catFrameFileNames = new Dictionary<int, string>()
        {
            { 0, "cat_move1.png" },
            { 1, "cat_move2.png" },
            { 2, "cat_move3.png" },
            { 3, "cat_killed.png" }
        };


        /// <summary>
        /// Base constructor for CatEntity class receiving position as parameter
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        public CatEntity(int x, int y)
            : base(new RectangleGeometry(new System.Windows.Rect(x, y, CatEntity.CatWidth, CatEntity.CatHeight)))
        {
            Active = true;
        }

        /// <summary>
        /// Gets the ImageBrush of the cat.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return new ImageBrush(GetBitmapImageByName(catFrameFileNames[Frame]));
            }
        }

        /// <summary>
        /// The cat's velocity on the x axis.
        /// </summary>
        public int VelocityX { get; set; }
        /// <summary>
        /// The current frame of the cat's animation.
        /// </summary>
        public int Frame { get => frame; set => frame = value; }
        /// <summary>
        /// Is the cat currently wandering on the road.
        /// </summary>
        public bool Active
        {
            get { return this.active; }
            set
            {
                this.active = value;
                this.frame = 0;
                this.VelocityX = 180;
            }
        }
    }
}
