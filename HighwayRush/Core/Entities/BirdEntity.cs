﻿// <copyright file="BirdEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities
{
    using System.Collections.Generic;
    using System.Windows.Media;
    using HighwayRush.Core.Entities.BaseEntities;

    /// <summary>
    /// Entity class defining information about a bird over the road.
    /// </summary>
    public class BirdEntity : GameEntity
    {
        /// <summary>
        /// The velocity on the y axis of the bird.
        /// </summary>
        public const int VelocityY = 350;

        /// <summary>
        /// The velocity on the x axis of the bird.
        /// </summary>
        public const int VelocityX = 150;

        /// <summary>
        /// The width of the bird.
        /// </summary>
        public const int BirdWidth = 12;

        /// <summary>
        /// The height of the bird.
        /// </summary>
        public const int BirdHeight = 20;

        /// <summary>
        /// Type of the bird.
        /// </summary>
        public enum BirdType
        {
            /// <summary>
            /// Bright colored bird
            /// </summary>
            Bright,
            /// <summary>
            /// Dark colored bird
            /// </summary>
            Dark
        }

        private static readonly Dictionary<BirdType, string> birdTypeFileNames = new Dictionary<BirdType, string>()
        {
            { BirdType.Bright, "bird_bright.png" },
            { BirdType.Dark, "bird_dark.png" }
        };

        /// <summary>
        /// The type of the bird.
        /// </summary>
        public BirdType BirdColorType { get; set; }

        /// <summary>
        /// Base constructor for BirdEntity class receiving position and type as parameter
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        /// <param name="type">Color type of the bird</param>
        public BirdEntity(int x, int y, BirdType type)
            : base(new RectangleGeometry(new System.Windows.Rect(x, y, BirdEntity.BirdWidth, BirdEntity.BirdHeight)))
        {
            this.BirdColorType = type;
            this.Active = true;
        }

        /// <summary>
        /// Gets the ImageBrush of the bird.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return new ImageBrush(GetBitmapImageByName(birdTypeFileNames[BirdColorType]));
            }
        }

        /// <summary>
        /// Is the bird currently flying over the map.
        /// </summary>
        public bool Active { get; set; }
    }
}
