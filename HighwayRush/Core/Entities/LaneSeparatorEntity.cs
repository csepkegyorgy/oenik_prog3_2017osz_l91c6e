﻿// <copyright file="LaneSeparatorEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities
{
    using System.Windows.Media;
    using HighwayRush.Core.Entities.BaseEntities;

    /// <summary>
    /// Entity class defining information about a lane separator of the road.
    /// </summary>
    public class LaneSeparatorEntity : GameEntity
    {
        /// <summary>
        /// The width of a lane separator.
        /// </summary>
        public const int LaneSeparatorWidth = 8;
        /// <summary>
        /// The height of a lane separator.
        /// </summary>
        public const int LaneSeparatorHeight = 30;

        /// <summary>
        /// Base constructor for LaneSeparatorEntity class receiving area as parameter
        /// </summary>
        /// <param name="area">Area of a lane separator</param>
        public LaneSeparatorEntity(Geometry area)
            : base(area)
        {
        }

        /// <summary>
        /// The color of a lane separator.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return Brushes.White;
            }
        }
    }
}
