﻿// <copyright file="BoostEntity.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush.Core.Entities
{
    using HighwayRush.Core.Entities.BaseEntities;
    using System.Collections.Generic;
    using System.Windows.Media;

    /// <summary>
    /// Entity class defining information about a booster object.
    /// </summary>
    public class BoostEntity : GameEntity
    {
        /// <summary>
        /// The width of the boost.
        /// </summary>
        public const int BoostWidth = 30;
        /// <summary>
        /// The height of the boost.
        /// </summary>
        public const int BoostHeight = 30;
        /// <summary>
        /// The velocity on the y axis of the boost.
        /// </summary>
        public const int VelocityY = 750;

        /// <summary>
        /// The type of the booster.
        /// </summary>
        public enum BoostType
        {
            /// <summary>
            /// Shield type
            /// </summary>
            Shield,
            /// <summary>
            /// Score type
            /// </summary>
            Score
        }

        private static readonly Dictionary<BoostType, string> boostTypeFileNames = new Dictionary<BoostType, string>()
        {
            { BoostType.Shield, "shield.png" },
            { BoostType.Score, "star.png" }
        };

        /// <summary>
        /// The type of the boost item.
        /// </summary>
        public BoostType TypeOfBoost { get; set; }

        /// <summary>
        /// Base constructor for BoostEntity class receiving position and type as parameter
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        /// <param name="type">The type of boost</param>
        public BoostEntity(int x, int y, BoostType type)
            : base(new RectangleGeometry(new System.Windows.Rect(x, y, BoostEntity.BoostWidth, BoostEntity.BoostHeight)))
        {
            this.TypeOfBoost = type;
        }

        /// <summary>
        /// Gets the ImageBrush of the boost.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return new ImageBrush(GetBitmapImageByName(boostTypeFileNames[TypeOfBoost]));
            }
        }
    }
}
