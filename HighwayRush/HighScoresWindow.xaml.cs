﻿// <copyright file="HighScoresWindow.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush
{
    using HighwayRush.View;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.IO;

    /// <summary>
    /// Interaction logic for HighScoresWindow.xaml
    /// The window loads the high scores and shows it.
    /// </summary>
    public partial class HighScoresWindow : Window
    {
        HighScoresViewModel vm;

        /// <summary>
        /// Base constructor for HighScoresWindow class
        /// </summary>
        public HighScoresWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vm = new HighScoresViewModel();
            vm.HighScores = new string[10];
            List<string> lines;
            try
            {
                lines = File.ReadAllLines("highscores.txt").ToList();
            }
            catch(FileNotFoundException)
            {
                lines = new List<string>();
            }
            List<string> scores = new List<string>();
            while(lines.Count != 0 && scores.Count != 10)
            {
                List<int> values = new List<int>();
                foreach(string line in lines)
                {
                    values.Add(int.Parse(line.Split(' ')[1]));
                }
                if(values.Count != 0 && lines.Count != 0)
                {
                    int maxIndex = values.IndexOf(values.Max());
                    scores.Add(lines[maxIndex]);
                    lines.RemoveAt(maxIndex);
                }
            }
            for (int i = 0; i < scores.Count; i++)
            {
                vm.HighScores[i] = scores[i].Split(' ')[0] + " - " + scores[i].Split(' ')[1] + " points in " + scores[i].Split(' ')[2] + " seconds";
            }

            this.DataContext = vm;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
