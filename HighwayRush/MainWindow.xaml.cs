﻿// <copyright file="MainWindow.xaml.cs" company="Csépke György">
// Copyright (c) Csépke György. All rights reserved.
// </copyright>

namespace HighwayRush
{
    using HighwayRush.Core.Entities;
    using HighwayRush.View;
    using System;
    using System.Windows;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// The window shows the main menu of the game.
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWindowViewModel vm;
        /// <summary>
        /// Base constructor for MainWindow class
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Window win = GetWindow(this);
            win.Close();
        }

        private void HighScores_Click(object sender, RoutedEventArgs e)
        {
            HighScoresWindow hsw = new HighScoresWindow();
            hsw.ShowDialog();
        }

        private void Play_Click(object sender, RoutedEventArgs e)
        {
            GameWindowViewModel gwvm = new GameWindowViewModel();
            gwvm.CarColorType = (CarEntity.CarType)Enum.Parse(typeof(CarEntity.CarType), vm.CarColorType.ToString());
            GameWindow game = new GameWindow(gwvm);

            game.Show();
            Window win = GetWindow(this);
            win.Close();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            vm = new MainWindowViewModel();
            DataContext = vm;
        }
    }
}
